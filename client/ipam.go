package client

import "strings"

// GetIPAdresses returns all IPAddresses on Netbox
func (client *NetboxClient) GetIPAdresses(limit int) ([]IPv4, error) {
	apiRequest := client.apiAddress + ipAddressPath
	results := []IPv4{}
	ipAddresses, err := client.GetAllData(limit, apiRequest)
	if err != nil {
		return nil, err
	}
	for _, entry := range ipAddresses {
		castEntry := entry.([]interface{})
		ip := IPv4{}
		for _, innerEntry := range castEntry {
			err := resultToStruct(innerEntry, &ip)
			if err != nil {
				return nil, err
			}
			results = append(results, ip)
		}
	}

	return results, nil
}

// CreateIPAddress creates an IpAddress on Netbox using the input data.
func (client *NetboxClient) CreateIPAddress(inputData IPv4) error {
	err := client.PostData(client.apiAddress+ipAddressPath, inputData)
	return err
}

// CreateIPAddress creates multiple IpAddresses on Netbox using the input data.
func (client *NetboxClient) CreateIPAddresses(inputData []IPv4) error {
	err := client.PostData(client.apiAddress+ipAddressPath, inputData)
	return err
}

// GetIPAddressByAddress returns an IPAddress from Netbox with matching IP.
func (client *NetboxClient) GetIPAddressByAddress(address string, limit int) (*IPv4, error) {
	allAddresses, err := client.GetIPAdresses(limit)
	if err != nil {
		return nil, err
	}
	for _, ip := range allAddresses {
		if strings.EqualFold(ip.Address, address) {
			return &ip, nil
		}
	}
	return nil, nil
}

// GetVRFs returns all VRFs from Netbox
func (client *NetboxClient) GetVRFs(limit int) ([]VRF, error) {
	apiRequest := client.apiAddress + vrfAddressPath
	results := []VRF{}
	VRFS, err := client.GetAllData(limit, apiRequest)
	if err != nil {
		return nil, err
	}
	for _, entry := range VRFS {
		castEntry := entry.([]interface{})
		ip := VRF{}
		for _, innerEntry := range castEntry {
			err := resultToStruct(innerEntry, &ip)
			if err != nil {
				return nil, err
			}
			results = append(results, ip)
		}
	}

	return results, nil
}

// CreateVRF creates a VRF on Netbox using the input data.
func (client *NetboxClient) CreateVRF(inputData VRF) error {
	err := client.PostData(client.apiAddress+vrfAddressPath, inputData)
	return err
}

// CreateVRF creates multiple VRFs on Netbox using the input data.
func (client *NetboxClient) CreateVRFs(inputData []VRF) error {
	err := client.PostData(client.apiAddress+vrfAddressPath, inputData)
	return err
}

// GetVRFByName returns a VRF from Netbox with matching name.
func (client *NetboxClient) GetVRFByName(name string, limit int) (*VRF, error) {
	allVRFs, err := client.GetVRFs(limit)
	if err != nil {
		return nil, err
	}
	for _, vrf := range allVRFs {
		if strings.EqualFold(vrf.Name, name) {
			return &vrf, nil
		}
	}
	return nil, nil
}
