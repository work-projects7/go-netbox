package client

import (
	"fmt"
	"strings"
)

// Get VMs from netbox, number of VMs pulled is based on limit
func (client *NetboxClient) GetVirtualMachines(limit int) ([]VirtualMachine, error) {
	apiRequest := client.apiAddress + virtualMachinePath
	results := []VirtualMachine{}
	virtualMachines, err := client.GetAllData(limit, apiRequest)
	if err != nil {
		return nil, err
	}
	for _, entry := range virtualMachines {
		castEntry := entry.([]interface{})
		for _, innerEntry := range castEntry {
			vm := VirtualMachine{}
			err := resultToStruct(innerEntry, &vm)
			if err != nil {
				return nil, err
			}
			results = append(results, vm)
		}
	}

	return results, nil
}

// Creates a netbox request to post a new VM
func (client *NetboxClient) CreateVirtualMachine(inputData VirtualMachine) error {
	err := client.PostData(client.apiAddress+virtualMachinePath, inputData)
	return err
}

// Creates a netbox request to patch a VM
func (client *NetboxClient) UpdateVirtualMachine(inputData VirtualMachine) error {
	idString := fmt.Sprintf("%.0f/", inputData.Id)
	err := client.UpdateData(client.apiAddress+virtualMachinePath+idString, inputData)
	return err
}

// Creates a netbox request to patch an array of VMs
func (client *NetboxClient) UpdateVirtualMachines(inputData []VirtualMachine) error {
	err := client.UpdateData(client.apiAddress+virtualMachinePath, inputData)
	return err
}

// Gets a VM from netbox based on it's name
func (client *NetboxClient) GetVirtualMachineByName(name string, limit int) (*VirtualMachine, error) {
	allVirtualMachines, err := client.GetVirtualMachines(limit)
	if err != nil {
		return nil, err
	}
	for _, vm := range allVirtualMachines {
		if strings.EqualFold(vm.Name, name) {
			return &vm, nil
		}
	}
	return nil, nil
}

// Creates a netbox request to post a new array of VMs
func (client *NetboxClient) CreateVirtualMachines(inputData []VirtualMachine) error {
	err := client.PostData(client.apiAddress+virtualMachinePath, inputData)
	return err
}

// Creates a netbox request to delete an array of VMs
func (client *NetboxClient) DeleteVirtualMachines(inputData []VirtualMachine) error {
	err := client.DeleteData(client.apiAddress+virtualMachinePath, inputData)
	return err
}
