package client

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"
)

// Virtualization Children
const clusterPath = "/api/virtualization/clusters/"
const clusterGroupsPath = "/api/virtualization/cluster-groups/"
const clusterTypesPath = "/api/virtualization/clusters-types/"
const virtualMachinePath = "/api/virtualization/virtual-machines/"

// ipam children
const ipAddressPath = "/api/ipam/ip-addresses/"
const vrfAddressPath = "/api/ipam/vrfs/"

// DCIM children
// const dcimPath = "/api/dcim/"
const devicePath = "/api/dcim/devices/"
const deviceRolePath = "/api/dcim/device-roles/"
const sitesPath = "/api/dcim/sites/"

// const userPath = "api/users/"

type NetboxClient struct {
	apiToken   string `yaml:"api_token"`
	apiAddress string `yaml:"api_address"`
	httpClient http.Client
}

// NewNetboxClient is the main constructor for NetboxClient.
func NewNetboxClient(apiToken string, apiAddress string) *NetboxClient {
	newClient := NetboxClient{
		apiToken:   apiToken,
		apiAddress: apiAddress,
		httpClient: http.Client{},
	}
	return &newClient
}

// PostData takes a struct marshals it to json and then makes a POST request to the client.
func (client NetboxClient) PostData(objectUrl string, inputData interface{}) error {
	payload, err := json.Marshal(inputData)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPost, objectUrl, bytes.NewBuffer(payload))
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Token "+client.apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, err := client.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("POST Request Response: %v\n", string(body))

	return nil
}

// DeleteData takes a struct marshals it to json and then makes a DELETE request to the client.
func (client NetboxClient) DeleteData(objectUrl string, inputData interface{}) error {
	payload, err := json.Marshal(inputData)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("DELETE", objectUrl, bytes.NewBuffer(payload))
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Token "+client.apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, err := client.httpClient.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("DELETE Request Response: %v\n", string(body))

	return nil
}

// UpdateData takes a struct marshals it to json and then makes a PATCH request to the client.
func (client NetboxClient) UpdateData(objectUrl string, inputData interface{}) error {
	payload, err := json.Marshal(inputData)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("PATCH", objectUrl, bytes.NewBuffer(payload))
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Token "+client.apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, err := client.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("PATCH Request Response: %v\n", string(body))

	return nil
}

// GetData makes a GET request to the netbox client for a range of data and unmarshals the data into a struct
func (client NetboxClient) GetData(limit int, apiPath string) (*NetboxJSONData, error) {
	jsonString := "?format=json"
	limitString := "?limit=" + strconv.Itoa(limit)

	apiRequest := apiPath
	if !strings.Contains(apiPath, "offset") {
		apiRequest = apiPath + jsonString + "&" + limitString
	}

	req, err := http.NewRequest("GET", apiRequest, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Token "+client.apiToken)

	resp, err := client.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	jsonData := NetboxJSONData{}

	err = json.Unmarshal(body, &jsonData) // Error occurs here doesn't expect < as first value
	if err != nil {
		return nil, err
	}

	return &jsonData, nil

}

// GetAllData utilizes GetData and loops through all pieces of data adding it to a list
func (client NetboxClient) GetAllData(limit int, apiPath string) ([]interface{}, error) {
	result := make([]interface{}, 0)
	initialResult, err := client.GetData(limit, apiPath)
	if err != nil {
		return nil, err
	}
	result = append(result, initialResult.Results)
	next := initialResult.Next
	for next != nil {
		initialResult, err = client.GetData(limit, *initialResult.Next)
		if err != nil {
			return nil, err
		}
		result = append(result, initialResult.Results)
		next = initialResult.Next
	}

	return result, nil
}

// resultToStruct takes an input and unmarshals the data into a destination struct
func resultToStruct(inputData interface{}, destination interface{}) error {
	jsonBytes, err := json.Marshal(inputData)
	if err != nil {
		return err
	}

	err = json.Unmarshal(jsonBytes, &destination)
	if err != nil {
		return err
	}

	return nil
}
