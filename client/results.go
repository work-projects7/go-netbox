package client

type NetboxJSONData struct {
	Next    *string     `json:"next"`
	Results interface{} `json:"results,omitempty"`
}

type BaseObject struct {
	CustomFields *map[string]interface{} `json:"custom_fields,omitempty"`
	Display      *string                 `json:"display,omitempty"`
	Id           float64                 `json:"id,omitempty"`
	Name         string                  `json:"name"`
	Tags         []interface{}           `json:"tags,omitempty"`
	URL          string                  `json:"url,omitempty"`
}

type Cluster struct {
	BaseObject
	DeviceCount         *float64   `json:"device_count,omitempty"`
	Type                *Reference `json:"type,omitempty"`
	Group               *Reference `json:"group,omitempty"`
	VirtualMachineCount *float64   `json:"virtualmachine_count,omitempty"`
	Comments            string     `json:"comments"`
	Site                *Reference `json:"site,omitempty"`
	Tenant              *Reference `json:"tenant,omitempty"`
}

type Device struct {
	BaseObject
	DeviceType       *DeviceType     `json:"device_type,omitempty"`
	DeviceRole       *Reference      `json:"device_role,omitempty"` //more vals
	Tenant           *Reference      `json:"tenant,omitempty"`
	Platform         *Reference      `json:"platform,omitempty"` //more vals
	Serial           string          `json:"serial,omitempty"`
	AssetTag         string          `json:"asset_tag,omitempty"`
	Site             *Reference      `json:"site,omitempty"`
	Location         *Reference      `json:"location,omitempty"` //more vals
	Rack             *Reference      `json:"rack,omitempty"`
	Position         float64         `json:"position,omitempty"`
	Face             *Status         `json:"face,omitempty"`
	ParentDevice     *Reference      `json:"parent_device,omitempty"`
	Airflow          *Status         `json:"airflow,omitempty"`
	PrimaryIP        *IP             `json:"primary_ip,omitempty"`
	PrimaryIPv6      *IP             `json:"primary_ip6,omitempty"`
	Cluster          *DeviceCluster  `json:"cluster,omitempty"`         //more vals
	VirtualChassis   *VirtualChassis `json:"virtual_chassis,omitempty"` //more vals
	VCPosition       float64         `json:"vc_position,omitempty"`
	VCPriority       float64         `json:"vc_priority,omitempty"`
	Comments         string          `json:"comments,omitempty"`
	LocalContextData string          `json:"local_context_data,omitempty"`
	ConfigContext    interface{}     `json:"config_context,omitempty"`
}

type DeviceCluster struct {
	Id float64 `json:"id,omitempty"`
	// Url  string  `json:"url,omitempty"`
	Name string `json:"name"`
}

type Descriptor struct {
	Id      float64 `json:"id,omitempty"`
	Url     string  `json:"url,omitempty"`
	Display *string `json:"display,omitempty"`
	Name    string  `json:"name"`
}

type DeviceType struct {
	Id           float64    `json:"id"`
	Manufacturer *Reference `json:"manufacturer,omitempty"` //more vals
	Model        string     `json:"model,omitempty"`
	Slug         *string    `json:"slug,omitempty"`
}

type IP struct {
	Id float64 `json:"id,omitempty"`
}

type VirtualChassis struct {
	Id      float64     `json:"id,omitempty"`
	Url     string      `json:"url,omitempty"`
	Display *string     `json:"display,omitempty"`
	Name    string      `json:"name,omitempty"`
	Master  *Descriptor `json:"master,omitempty"`
}

type Reference struct {
	Id   float64 `json:"id,omitempty"`
	Name string  `json:"name"`
	Slug *string `json:"slug,omitempty"`
}

// type SiteReference struct {
// 	Name  string `json:"type"`
// 	Slug string `json:"value"`
// }

type Status struct {
	Label *string `json:"label,omitempty"`
	Value string  `json:"value"`
}

type VirtualMachine struct {
	BaseObject
	PrimaryIPv4 *IPv4      `json:"primary_ip4,omitempty"`
	Cluster     *Reference `json:"cluster,omitempty"`
	VCPUs       int        `json:"vcpus,omitempty"`
	Memory      int        `json:"memory,omitempty"` //Memory in MB
	Disk        int        `json:"disk,omitempty"`   //Disk space in GB
	Site        *Reference `json:"site,omitempty"`
	Device      *Reference `json:"device"`
	Platform    *Reference `json:"platform,omitempty"`
	Role        *Reference `json:"role,omitempty"`
	Status      *Status    `json:"status,omitempty"`
	Tenant      *Reference `json:"tenant,omitempty"`
}

type IPv4 struct {
	Address string  `json:"address"`
	Id      float64 `json:"id"`
	Status  *Status `json:"status,omitempty"`
	Url     string  `json:"url"`
	//Family  int     `json:"family"`
	DNSName *string `json:"dns_name,omitempty"`
	VRF     *VRF    `json:"vrf,omitempty"`
}

type VRF struct {
	Id            float64 `json:"id"`
	Url           string  `json:"url"`
	Name          string  `json:"name"`
	Tenant        *Reference
	IPCount       *int `json:"ipaddress_count,omitempty"`
	PrefixCount   *int `json:"prefix_count,omitempty"`
	EnforceUnique bool `json:"enforce_unique"`
}
