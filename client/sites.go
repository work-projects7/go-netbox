package client

import "strings"

// GetSites returns all Sites from Netbox.
func (client *NetboxClient) GetSites(limit int) ([]Reference, error) {
	apiRequest := client.apiAddress + sitesPath
	results := []Reference{}
	sites, err := client.GetAllData(limit, apiRequest)
	if err != nil {
		return nil, err
	}
	for _, entry := range sites {
		castEntry := entry.([]interface{})
		site := Reference{}
		for _, innerEntry := range castEntry {
			err := resultToStruct(innerEntry, &site)
			if err != nil {
				return nil, err
			}
			results = append(results, site)
		}
	}
	return results, nil
}

// GetSiteByName returns a Site from Netbox with matching name.
func (client *NetboxClient) GetSiteByName(name string, limit int) (*Reference, error) {
	allSites, err := client.GetSites(limit)
	if err != nil {
		return nil, err
	}
	for _, site := range allSites {
		if strings.EqualFold(site.Name, name) {
			return &site, nil
		}
	}
	return nil, nil
}
