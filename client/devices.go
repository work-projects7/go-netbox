package client

import (
	"fmt"
	"strings"
)

// Creates a netbox request to get an array of devices
func (client *NetboxClient) GetDevices(limit int) ([]Device, error) {
	apiRequest := client.apiAddress + devicePath
	results := []Device{}
	devices, err := client.GetAllData(limit, apiRequest)
	if err != nil {
		return nil, err
	}
	for _, entry := range devices {
		castEntry := entry.([]interface{})
		for _, innerEntry := range castEntry {
			device := Device{}
			err := resultToStruct(innerEntry, &device)
			if err != nil {
				return nil, err
			}
			results = append(results, device)
		}
	}

	return results, nil
}

// Creates a netbox request to post a new device
func (client *NetboxClient) CreateDevice(inputData Device) error {
	err := client.PostData(client.apiAddress+devicePath, inputData)
	return err
}

// Creates a netbox request to post a new array of devices
func (client *NetboxClient) CreateDevices(inputData []Device) error {
	err := client.PostData(client.apiAddress+devicePath, inputData)
	return err
}

// Creates a netbox request to patch a device
func (client *NetboxClient) UpdateDevice(inputData Device) error {
	idString := fmt.Sprintf("%.0f/", inputData.Id)
	err := client.UpdateData(client.apiAddress+devicePath+idString, inputData)
	return err
}

// Creates a netbox request to patch an array of devices
func (client *NetboxClient) UpdateDevices(inputData []Device) error {
	err := client.UpdateData(client.apiAddress+devicePath, inputData)
	return err
}

// Creates a netbox request to delete an array of devices
func (client *NetboxClient) DeleteDevices(inputData []Device) error {
	err := client.DeleteData(client.apiAddress+devicePath, inputData)
	return err
}

// Creates a netbox request to get a device by name
func (client *NetboxClient) GetDeviceByName(name string, limit int) (*Device, error) {
	allDevices, err := client.GetDevices(limit)
	if err != nil {
		return nil, err
	}
	for _, device := range allDevices {
		if strings.EqualFold(device.Name, name) {
			return &device, nil
		}
	}
	return nil, nil
}
