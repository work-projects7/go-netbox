package client

import "strings"

// GetRoles returns all Roles from Netbox.
func (client *NetboxClient) GetRoles(limit int) ([]Reference, error) {
	apiRequest := client.apiAddress + deviceRolePath
	results := []Reference{}
	netboxRoles, err := client.GetAllData(limit, apiRequest)
	if err != nil {
		return nil, err
	}
	for _, entry := range netboxRoles {
		castEntry := entry.([]interface{})
		role := Reference{}
		for _, innerEntry := range castEntry {
			err := resultToStruct(innerEntry, &role)
			if err != nil {
				return nil, err
			}
			results = append(results, role)
		}
	}

	return results, nil
}

// GetRoleByName returns a Role from Netbox with matching name.
func (client *NetboxClient) GetRoleByName(name string, limit int) (*Reference, error) {
	allRoles, err := client.GetRoles(limit)
	if err != nil {
		return nil, err
	}
	for _, role := range allRoles {
		if strings.EqualFold(role.Name, name) {
			return &role, nil
		}
	}
	return nil, nil
}
