package client

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

var JSONClusterDataArr NetboxJSONData
var JSONVMDataArr NetboxJSONData
var JSONDeviceDataArr NetboxJSONData

// TODO: Unfinished
func TestClusterData(t *testing.T) {
	netboxClient := fakeNetbox()

	// Cluster 1 test data
	clustName1 := "TestClust1"
	typeName1 := "VMWare"
	groupName1 := "Company1"
	tenantName1 := "Dept1"
	devCount1 := 1.0
	vmCount1 := 1.0
	customField1 := map[string]interface{}{"vCenter Server": "test-vcenter1"}

	clusterData1 := Cluster{
		BaseObject: BaseObject{
			CustomFields: &customField1,
			Display:      &clustName1,
			Id:           105,
			Name:         clustName1,
		},
		DeviceCount: &devCount1,
		Type: &Reference{
			Id:   1,
			Name: typeName1,
			Slug: &typeName1,
		},
		Group: &Reference{
			Id:   1,
			Name: groupName1,
			Slug: &groupName1,
		},
		VirtualMachineCount: &vmCount1,
		Comments:            "",
		Site:                nil,
		Tenant: &Reference{
			Id:   1,
			Name: tenantName1,
			Slug: &tenantName1,
		},
	}

	//	cluster 2 data
	clustName2 := "TestClust2"
	typeName2 := "VMWare"
	groupName2 := "Company2"
	tenantName2 := "Dept2"
	devCount2 := 3.0
	vmCount2 := 3.0
	customField2 := map[string]interface{}{"vCenter Server": "test-vcenter2"}

	clusterData2 := Cluster{
		BaseObject: BaseObject{
			CustomFields: &customField2,
			Display:      &clustName2,
			Id:           105,
			Name:         clustName2,
		},
		DeviceCount: &devCount2,
		Type: &Reference{
			Id:   1,
			Name: typeName2,
			Slug: &typeName2,
		},
		Group: &Reference{
			Id:   1,
			Name: groupName2,
			Slug: &groupName2,
		},
		VirtualMachineCount: &vmCount2,
		Comments:            "",
		Site:                nil,
		Tenant: &Reference{
			Id:   1,
			Name: tenantName2,
			Slug: &tenantName2,
		},
	}

	// Testing CreateClusters
	ClusterArr := []Cluster{clusterData1}

	err := netboxClient.CreateClusters(ClusterArr)
	if err != nil {
		t.Fatalf("Failed to CreateClusters: %v\n", err)
	}

	// Testing GetClusters
	getRaw, err := netboxClient.GetClusters(30)
	if err != nil {
		t.Fatalf("Failed to GetClusters: %v\n", err)
	}
	get, err := json.Marshal(getRaw)
	if err != nil {
		t.Fatalf("Error Marshaling GetClusters raw data: %v\n", err)
	}

	// Check to make sure get data is correct. Expect ClusterArr
	ClusterArrJson, err := json.Marshal(ClusterArr)
	if err != nil {
		t.Fatalf("Error marshaling ClusterArr: %v\n", err)
	}
	if string(ClusterArrJson) != string(get) {
		fmt.Println("ClusterArrJson and Get values are different")
		t.Fail()
	}

	// Testing UpdateClusters
	UpdateClusterArr := []Cluster{clusterData2}

	err = netboxClient.UpdateClusters(UpdateClusterArr)
	if err != nil {
		t.Fatalf("Failed to update Clusters: %v\n", err)
	}

	// Get update data
	getUpdateRaw, err := netboxClient.GetClusters(30)
	if err != nil {
		t.Fatalf("Failed to GetClusters: %v\n", err)
	}
	getUpdate, err := json.Marshal(getUpdateRaw)
	if err != nil {
		t.Fatalf("Error Marshaling GetClusters raw update data: %v\n", err)
	}

	// Check get updated data. Expect UpdateClusterArr
	clusterUpdateArrJson, err := json.Marshal(UpdateClusterArr)
	if err != nil {
		t.Fatalf("Error marshaling ClusterArr: %v\n", err)
	}
	if string(clusterUpdateArrJson) != string(getUpdate) {
		fmt.Println("clusterArrJson and Get values are different")
		t.Fail()
	}
}

func TestVirtualMachineData(t *testing.T) {
	netboxClient := fakeNetbox()

	// virtual machine 1 test data
	VMName1 := "TestVM1"
	VMIPv4Label := "IPv4Label1"
	VMIPv4DNS := "IPv4DNS1"
	clustName1 := "Cluster1"
	siteName1 := "site1"
	platformName1 := "platform1"
	roleName1 := "role1"
	status1 := "status1"
	tenantName1 := "Dept1"
	IPv4IPCount1 := 1
	IPv4PrefixCount1 := 1
	customField1 := map[string]interface{}{"vCenter Server": "test-vcenter1"}

	VMData1 := VirtualMachine{
		BaseObject: BaseObject{
			CustomFields: &customField1,
			Display:      &VMName1,
			Id:           105,
			Name:         VMName1,
		},
		PrimaryIPv4: &IPv4{
			Address: "address1",
			Id:      1.0,
			Status: &Status{
				Label: &VMIPv4Label,
				Value: "IPv4Value1",
			},
			Url:     "IPv4Url1",
			DNSName: &VMIPv4DNS,
			VRF: &VRF{
				Id:   1.0,
				Url:  "IPv4VRFUrl1",
				Name: "IPv4VRF1",
				Tenant: &Reference{
					Id:   1.0,
					Name: tenantName1,
					Slug: &tenantName1,
				},
				IPCount:       &IPv4IPCount1,
				PrefixCount:   &IPv4PrefixCount1,
				EnforceUnique: true,
			},
		},
		Cluster: &Reference{
			Id:   1.0,
			Name: clustName1,
			Slug: &clustName1,
		},
		VCPUs:  1,
		Memory: 1,
		Disk:   1,
		Site: &Reference{
			Id:   1.0,
			Name: siteName1,
			Slug: &siteName1,
		},
		Platform: &Reference{
			Id:   1.0,
			Name: platformName1,
			Slug: &platformName1,
		},
		Role: &Reference{
			Id:   1.0,
			Name: roleName1,
			Slug: &roleName1,
		},
		Status: &Status{
			Label: &status1,
			Value: status1,
		},
		Tenant: &Reference{
			Id:   1,
			Name: tenantName1,
			Slug: &tenantName1,
		},
	}

	//	virtual machine 2 data
	VMName2 := "TestVM1"
	VMIPv4Label2 := "IPv4Label2"
	VMIPv4DNS2 := "IPv4DNS2"
	clustName2 := "Cluster2"
	siteName2 := "site2"
	platformName2 := "platform2"
	roleName2 := "role2"
	status2 := "status2"
	tenantName2 := "Dept2"
	IPv4IPCount2 := 2
	IPv4PrefixCount2 := 2
	customField2 := map[string]interface{}{"vCenter Server": "test-vcenter2"}

	VMData2 := VirtualMachine{
		BaseObject: BaseObject{
			CustomFields: &customField2,
			Display:      &VMName2,
			Id:           105,
			Name:         VMName2,
		},
		PrimaryIPv4: &IPv4{
			Address: "address2",
			Id:      2.0,
			Status: &Status{
				Label: &VMIPv4Label2,
				Value: "IPv4Value2",
			},
			Url:     "IPv4Url2",
			DNSName: &VMIPv4DNS2,
			VRF: &VRF{
				Id:   2.0,
				Url:  "IPv4VRFUrl2",
				Name: "IPv4VRF2",
				Tenant: &Reference{
					Id:   2.0,
					Name: tenantName2,
					Slug: &tenantName2,
				},
				IPCount:       &IPv4IPCount2,
				PrefixCount:   &IPv4PrefixCount2,
				EnforceUnique: true,
			},
		},
		Cluster: &Reference{
			Id:   2.0,
			Name: clustName2,
			Slug: &clustName2,
		},
		VCPUs:  2,
		Memory: 2,
		Disk:   2,
		Site: &Reference{
			Id:   2.0,
			Name: siteName2,
			Slug: &siteName2,
		},
		Platform: &Reference{
			Id:   2.0,
			Name: platformName2,
			Slug: &platformName2,
		},
		Role: &Reference{
			Id:   2.0,
			Name: roleName2,
			Slug: &roleName2,
		},
		Status: &Status{
			Label: &status2,
			Value: status2,
		},
		Tenant: &Reference{
			Id:   2,
			Name: tenantName2,
			Slug: &tenantName2,
		},
	}

	// Testing CreateVirtualMachines
	VirtualMachineArr := []VirtualMachine{VMData1}

	err := netboxClient.CreateVirtualMachines(VirtualMachineArr)
	if err != nil {
		t.Fatalf("Failed to CreateVirtualMachines %v\n", err)
	}

	// Testing GetVirtualMachines
	getRaw, err := netboxClient.GetVirtualMachines(30)
	if err != nil {
		t.Fatalf("Failed to GetVirtualMachines: %v\n", err)
	}
	get, err := json.Marshal(getRaw)
	if err != nil {
		t.Fatalf("Error Marshaling GetVirtualMachines raw data: %v\n", err)
	}

	// Check to make sure get data is correct. Expect VirtualMachineArr
	VirtualMachineArrJson, err := json.Marshal(VirtualMachineArr)
	if err != nil {
		t.Fatalf("Error marshaling VirtualMachineArr: %v\n", err)
	}
	if string(VirtualMachineArrJson) != string(get) {
		fmt.Println("VirtualMachineArrJson and Get values are different")
		t.Fail()
	}

	// Testing UpdateVirtualMAchines
	UpdateVirtualMachineArr := []VirtualMachine{VMData2}

	err = netboxClient.UpdateVirtualMachines(UpdateVirtualMachineArr)
	if err != nil {
		t.Fatalf("Failed to update Virtual Machines: %v\n", err)
	}

	// Get update data
	getUpdateRaw, err := netboxClient.GetVirtualMachines(30)
	if err != nil {
		t.Fatalf("Failed to GetVirtualMachines: %v\n", err)
	}
	getUpdate, err := json.Marshal(getUpdateRaw)
	if err != nil {
		t.Fatalf("Error Marshaling GetVirtualMachines raw update data: %v\n", err)
	}

	// Check get updated data. Expect UpdateVirtualMachineArr
	VirtualMachineUpdateArrJson, err := json.Marshal(UpdateVirtualMachineArr)
	if err != nil {
		t.Fatalf("Error marshaling UpdateVirtualMachineArr: %v\n", err)
	}
	if string(VirtualMachineUpdateArrJson) != string(getUpdate) {
		fmt.Println("VirtualMachineArrJson and Get values are different")
		t.Fail()
	}
}

func TestDeviceData(t *testing.T) {
	netboxClient := fakeNetbox()

	// Device 1 test data
	deviceName1 := "TestDevice1"
	typeManufacturerName1 := "manufacturer1"
	typeModel1 := "model1"
	roleName1 := "deviceRole1"
	tenantName1 := "Dept1"
	platformName1 := "platform1"
	siteName1 := "site1"
	locationName1 := "location1"
	rackName1 := "rack1"
	position1 := 1.0
	face1 := "face1"
	parentDevice1 := "parent1"
	airflow1 := "airflow1"
	virtualChassisName1 := "chassis1"
	virtualChassisMasterName1 := "masterChassis1"
	customField1 := map[string]interface{}{"vCenter Server": "test-vcenter1"}
	type context1 interface {
		context() string
	}
	var configContext1 context1

	DeviceData1 := Device{
		BaseObject: BaseObject{
			CustomFields: &customField1,
			Display:      &deviceName1,
			Id:           105,
			Name:         deviceName1,
		},
		DeviceType: &DeviceType{
			Id: 1.0,
			Manufacturer: &Reference{
				Id:   1.0,
				Name: typeManufacturerName1,
				Slug: &typeManufacturerName1,
			},
			Model: typeModel1,
			Slug:  &typeModel1,
		},
		DeviceRole: &Reference{
			Id:   1.0,
			Name: roleName1,
			Slug: &roleName1,
		},
		Tenant: &Reference{
			Id:   1,
			Name: tenantName1,
			Slug: &tenantName1,
		},
		Platform: &Reference{
			Id:   1.0,
			Name: platformName1,
			Slug: &platformName1,
		},
		Serial:   "serial1",
		AssetTag: "asset1",
		Site: &Reference{
			Id:   1.0,
			Name: siteName1,
			Slug: &siteName1,
		},
		Location: &Reference{
			Id:   1.0,
			Name: locationName1,
			Slug: &locationName1,
		},
		Rack: &Reference{
			Id:   1.0,
			Name: rackName1,
			Slug: &rackName1,
		},
		Position: position1,
		Face: &Status{
			Label: &face1,
			Value: face1,
		},
		ParentDevice: &Reference{
			Id:   1.0,
			Name: parentDevice1,
			Slug: &parentDevice1,
		},
		Airflow: &Status{
			Label: &airflow1,
			Value: airflow1,
		},
		PrimaryIP: &IP{
			Id: 1.0,
		},
		PrimaryIPv6: &IP{
			Id: 1.0,
		},
		Cluster: &DeviceCluster{
			Id:   1.0,
			Name: "Cluster1",
		},
		VirtualChassis: &VirtualChassis{
			Id:      1.0,
			Url:     "VirtualChassisUrl1",
			Name:    virtualChassisName1,
			Display: &virtualChassisName1,
			Master: &Descriptor{
				Id:      1.0,
				Url:     "VirtualChassisMasterUrl1",
				Display: &virtualChassisMasterName1,
				Name:    virtualChassisMasterName1,
			},
		},
		VCPosition:       1.0,
		VCPriority:       1.0,
		Comments:         "comment1",
		LocalContextData: "localContext1",
		ConfigContext:    configContext1,
	}

	//	Device 2 data
	deviceName2 := "TestDevice1"
	typeManufacturerName2 := "manufacturer1"
	typeModel2 := "model1"
	roleName2 := "deviceRole1"
	tenantName2 := "Dept1"
	platformName2 := "platform1"
	siteName2 := "site1"
	locationName2 := "location1"
	rackName2 := "rack1"
	position2 := 1.0
	face2 := "face1"
	parentDevice2 := "parent1"
	airflow2 := "airflow1"
	virtualChassisName2 := "chassis1"
	virtualChassisMasterName2 := "masterChassis1"
	customField2 := map[string]interface{}{"vCenter Server": "test-vcenter1"}
	var configContext2 context1

	DeviceData2 := Device{
		BaseObject: BaseObject{
			CustomFields: &customField2,
			Display:      &deviceName2,
			Id:           2.0,
			Name:         deviceName2,
		},
		DeviceType: &DeviceType{
			Id: 2.0,
			Manufacturer: &Reference{
				Id:   2.0,
				Name: typeManufacturerName2,
				Slug: &typeManufacturerName2,
			},
			Model: typeModel2,
			Slug:  &typeModel2,
		},
		DeviceRole: &Reference{
			Id:   2.0,
			Name: roleName2,
			Slug: &roleName2,
		},
		Tenant: &Reference{
			Id:   2,
			Name: tenantName2,
			Slug: &tenantName2,
		},
		Platform: &Reference{
			Id:   2.0,
			Name: platformName2,
			Slug: &platformName2,
		},
		Serial:   "serial2",
		AssetTag: "asset2",
		Site: &Reference{
			Id:   2.0,
			Name: siteName2,
			Slug: &siteName2,
		},
		Location: &Reference{
			Id:   2.0,
			Name: locationName2,
			Slug: &locationName2,
		},
		Rack: &Reference{
			Id:   2.0,
			Name: rackName2,
			Slug: &rackName2,
		},
		Position: position2,
		Face: &Status{
			Label: &face2,
			Value: face2,
		},
		ParentDevice: &Reference{
			Id:   2.0,
			Name: parentDevice2,
			Slug: &parentDevice2,
		},
		Airflow: &Status{
			Label: &airflow2,
			Value: airflow2,
		},
		PrimaryIP: &IP{
			Id: 2.0,
		},
		PrimaryIPv6: &IP{
			Id: 2.0,
		},
		Cluster: &DeviceCluster{
			Id:   2.0,
			Name: "Cluster2",
		},
		VirtualChassis: &VirtualChassis{
			Id:      2.0,
			Url:     "VirtualChassisUrl2",
			Name:    virtualChassisName2,
			Display: &virtualChassisName2,
			Master: &Descriptor{
				Id:      2.0,
				Url:     "VirtualChassisMasterUrl2",
				Display: &virtualChassisMasterName2,
				Name:    virtualChassisMasterName2,
			},
		},
		VCPosition:       2.0,
		VCPriority:       2.0,
		Comments:         "comment2",
		LocalContextData: "localContext2",
		ConfigContext:    configContext2,
	}

	// Testing CreateDevices
	DeviceArr := []Device{DeviceData1}

	err := netboxClient.CreateDevices(DeviceArr)
	if err != nil {
		t.Fatalf("Failed to CreateDevices: %v\n", err)
	}

	// Testing GetDevices
	getRaw, err := netboxClient.GetDevices(30)
	if err != nil {
		t.Fatalf("Failed to GetDevices: %v\n", err)
	}
	get, err := json.Marshal(getRaw)
	if err != nil {
		t.Fatalf("Error Marshaling GetDevices raw data: %v\n", err)
	}

	// Check to make sure get data is correct. Expect DeviceArr
	DeviceArrJson, err := json.Marshal(DeviceArr)
	if err != nil {
		t.Fatalf("Error marshaling DeviceArr: %v\n", err)
	}
	if string(DeviceArrJson) != string(get) {
		fmt.Println("DeviceArrJson and Get values are different")
		t.Fail()
	}

	// Testing UpdateDevices
	UpdateDeviceArr := []Device{DeviceData2}

	err = netboxClient.UpdateDevices(UpdateDeviceArr)
	if err != nil {
		t.Fatalf("Failed to update Devices: %v\n", err)
	}

	// Get update data
	getUpdateRaw, err := netboxClient.GetDevices(30)
	if err != nil {
		t.Fatalf("Failed to GetDevices: %v\n", err)
	}
	getUpdate, err := json.Marshal(getUpdateRaw)
	if err != nil {
		t.Fatalf("Error Marshaling GetDevices raw update data: %v\n", err)
	}

	// Check get updated data. Expect UpdateDeviceArr
	DeviceUpdateArrJson, err := json.Marshal(UpdateDeviceArr)
	if err != nil {
		t.Fatalf("Error marshaling DeviceArr: %v\n", err)
	}
	if string(DeviceUpdateArrJson) != string(getUpdate) {
		fmt.Println("DeviceArrJson and Get values are different")
		t.Fail()
	}
}

func fakeNetbox() *NetboxClient {
	mux := http.NewServeMux()

	mux.HandleFunc("/api/virtualization/clusters/", testClusterHandler)
	mux.HandleFunc("/api/virtualization/virtual-machines/", testVirtualMachineHandler)
	mux.HandleFunc("/api/dcim/devices/", testDeviceHandler)

	mockServer := httptest.NewServer(mux)
	servURL := mockServer.URL
	netboxClient := NewNetboxClient("", servURL)

	return netboxClient
}

func testClusterHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" || r.Method == "PATCH" {
		// grabs write data
		reqBody, err := io.ReadAll(r.Body)
		if err != nil {
			log.Fatalf("Problem reading request")
		}

		ClusterArrStorage := []Cluster{}

		// pushes write data into array of Clusters
		err = json.Unmarshal([]byte(reqBody), &ClusterArrStorage)
		if err != nil {
			log.Fatalf("Error unmarshaling Cluster data: %v\n", err)
		}

		// build proper interface to store in json file
		jsonData := NetboxJSONData{
			Next:    nil,
			Results: ClusterArrStorage,
		}

		// Write NetboxJSONData to file and write cluster data to request
		JSONClusterDataArr = jsonData
		_, err = w.Write([]byte(reqBody))
		if err != nil {
			log.Fatalf("Error writing to console: %v\n", err)
		}
	} else if r.Method == "GET" {
		readData, err := json.Marshal(JSONClusterDataArr)
		if err != nil {
			log.Fatalf("JSONClusterDataArr failed to marshal: %v\n", err)
		}
		_, err = w.Write([]byte(readData))
		if err != nil {
			log.Fatalf("Error writing to console: %v\n", err)
		}
	}
}

func testVirtualMachineHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" || r.Method == "PATCH" {
		// grabs write data
		reqBody, err := io.ReadAll(r.Body)
		if err != nil {
			log.Fatalf("Problem reading request")
		}

		VMArrStorage := []VirtualMachine{}

		// pushes write data into array of Clusters
		err = json.Unmarshal([]byte(reqBody), &VMArrStorage)
		if err != nil {
			log.Fatalf("Error unmarshaling Cluster data: %v\n", err)
		}

		// build proper interface to store in json file
		jsonData := NetboxJSONData{
			Next:    nil,
			Results: VMArrStorage,
		}

		// Write NetboxJSONData to file and write cluster data to request
		JSONVMDataArr = jsonData
		_, err = w.Write([]byte(reqBody))
		if err != nil {
			log.Fatalf("Error writing to console: %v\n", err)
		}
	} else if r.Method == "GET" {
		readData, err := json.Marshal(JSONVMDataArr)
		if err != nil {
			log.Fatalf("JSONVMDataArr failed to marshal: %v\n", err)
		}
		_, err = w.Write([]byte(readData))
		if err != nil {
			log.Fatalf("Error writing to console: %v\n", err)
		}
	}
}

func testDeviceHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" || r.Method == "PATCH" {
		// grabs write data
		reqBody, err := io.ReadAll(r.Body)
		if err != nil {
			log.Fatalf("Problem reading request")
		}

		DeviceArrStorage := []Device{}

		// pushes write data into array of Clusters
		err = json.Unmarshal([]byte(reqBody), &DeviceArrStorage)
		if err != nil {
			log.Fatalf("Error unmarshaling Cluster data: %v\n", err)
		}

		// build proper interface to store in json file
		jsonData := NetboxJSONData{
			Next:    nil,
			Results: DeviceArrStorage,
		}

		// Write NetboxJSONData to file and write cluster data to request
		JSONDeviceDataArr = jsonData
		_, err = w.Write([]byte(reqBody))
		if err != nil {
			log.Fatalf("Error writing to console: %v\n", err)
		}
	} else if r.Method == "GET" {
		readData, err := json.Marshal(JSONDeviceDataArr)
		if err != nil {
			log.Fatalf("JSONDeviceDataArr failed to marshal: %v\n", err)
		}
		_, err = w.Write([]byte(readData))
		if err != nil {
			log.Fatalf("Error writing to console: %v\n", err)
		}
	}
}
