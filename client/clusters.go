package client

import (
	"fmt"
	"strings"
)

// Creates a netbox request to get an array of clusters
func (client *NetboxClient) GetClusters(limit int) ([]Cluster, error) {
	apiRequest := client.apiAddress + clusterPath
	results := []Cluster{}
	netboxClusters, err := client.GetAllData(limit, apiRequest)
	if err != nil {
		return nil, err
	}
	for _, entry := range netboxClusters {
		castEntry := entry.([]interface{})
		for _, innerEntry := range castEntry {
			cluster := Cluster{}
			err := resultToStruct(innerEntry, &cluster)
			if err != nil {
				return nil, err
			}
			results = append(results, cluster)
		}
	}

	return results, nil
}

// Creates a netbox request to get an array of cluster types
func (client *NetboxClient) GetClusterTypes(limit int) ([]Reference, error) {
	apiRequest := client.apiAddress + clusterTypesPath
	results := []Reference{}
	netboxClusters, err := client.GetAllData(limit, apiRequest)
	if err != nil {
		return nil, err
	}
	for _, entry := range netboxClusters {
		castEntry := entry.([]interface{})
		clusterType := Reference{}
		for _, innerEntry := range castEntry {
			err := resultToStruct(innerEntry, &clusterType)
			if err != nil {
				return nil, err
			}
			results = append(results, clusterType)
		}
	}
	return results, nil
}

// Creates a netbox request to get an array of cluster groups
func (client *NetboxClient) GetClusterGroups(limit int) ([]Reference, error) {
	apiRequest := client.apiAddress + clusterGroupsPath
	results := []Reference{}
	netboxClusterGroups, err := client.GetAllData(limit, apiRequest)
	if err != nil {
		return nil, err
	}
	for _, entry := range netboxClusterGroups {
		castEntry := entry.([]interface{})
		clusterGroup := Reference{}
		for _, innerEntry := range castEntry {
			err := resultToStruct(innerEntry, &clusterGroup)
			if err != nil {
				return nil, err
			}
			results = append(results, clusterGroup)
		}
	}
	return results, nil
}

// Creates a netbox request to post a cluster
func (client *NetboxClient) CreateCluster(inputData Cluster) error {
	err := client.PostData(client.apiAddress+clusterPath, inputData)
	return err
}

// Creates a netbox request to patch a cluster
func (client *NetboxClient) UpdateCluster(inputData Cluster) error {
	idString := fmt.Sprintf("%.0f/", inputData.Id)
	err := client.UpdateData(client.apiAddress+clusterPath+idString, inputData)
	return err
}

// Creates a netbox request to patch an array of clusters
func (client *NetboxClient) UpdateClusters(inputData []Cluster) error {
	err := client.UpdateData(client.apiAddress+clusterPath, inputData)
	return err
}

// Creates a netbox request to get a cluster by it's name
func (client *NetboxClient) GetClusterByName(name string, limit int) (*Cluster, error) {
	allClusters, err := client.GetClusters(limit)
	if err != nil {
		return nil, err
	}
	for _, cluster := range allClusters {
		if strings.EqualFold(cluster.Name, name) {
			return &cluster, nil
		}
	}
	return nil, nil
}

// Creates a netbox request to get a cluster type by the cluster name
func (client *NetboxClient) GetClusterTypeByName(name string, limit int) (*Reference, error) {
	allClusterTypes, err := client.GetClusterTypes(limit)
	if err != nil {
		return nil, err
	}
	for _, clusterType := range allClusterTypes {
		if strings.EqualFold(clusterType.Name, name) {
			return &clusterType, nil
		}
	}
	return nil, nil
}

// Creates a netbox request to get a cluster group by the cluster name
func (client *NetboxClient) GetClusterGroupByName(name string, limit int) (*Reference, error) {
	allClusterGroups, err := client.GetClusterGroups(limit)
	if err != nil {
		return nil, err
	}
	for _, clusterType := range allClusterGroups {
		if strings.EqualFold(clusterType.Name, name) {
			return &clusterType, nil
		}
	}
	return nil, nil
}

// Creates a netbox request to post a new array of clusters
func (client *NetboxClient) CreateClusters(inputData []Cluster) error {
	err := client.PostData(client.apiAddress+clusterPath, inputData)
	return err
}

// Creates a netbox request to delete an array of clusters
func (client *NetboxClient) DeleteClusters(inputData []Cluster) error {
	err := client.DeleteData(client.apiAddress+clusterPath, inputData)
	return err
}
