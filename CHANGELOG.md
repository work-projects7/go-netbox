### Bug Fixes

* removed omitempty from device in vm struct

### Bug Fixes

* added device to virtualmachine struct and fixed deprecated ioutil
* added device to virtualmachine struct and fixed deprecated ioutil

### Features

* updated gonetbox with the most relevant changes after the netbox updates

### Features

* added delete devices functionality

### ⚠ BREAKING CHANGES

* compatible with netbox v3.2.5 and added testing

### Features

* compatible with netbox v3.2.5 and added testing
